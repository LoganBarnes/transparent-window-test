# ##############################################################################
# Copyright (c) 2022 Logan Barnes - All Rights Reserved
# ##############################################################################

### System Packages ###
find_package(OpenGL REQUIRED)

### External Repositories ###
cpmaddpackage("gh:gabime/spdlog@1.9.2")
cpmaddpackage(
  NAME GLFW
  GIT_REPOSITORY https://github.com/glfw/glfw.git
  GIT_TAG 3.3.8
  OPTIONS
  "GLFW_BUILD_TESTS OFF"
  "GLFW_BUILD_EXAMPLES OFF"
  "GLFW_BUILD_DOCS OFF"
)

if (spdlog_ADDED)
  # Mark external include directories as system includes to avoid warnings.
  target_include_directories(
    spdlog
    SYSTEM
    INTERFACE
    $<BUILD_INTERFACE:${spdlog_SOURCE_DIR}/include>
  )
endif ()

if (GLFW_ADDED)
  add_library(
    glfw::glfw
    ALIAS
    glfw
  )
endif ()
