// ///////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Logan Barnes - All Rights Reserved
// ///////////////////////////////////////////////////////////////////////////////////////
#version 410

layout(location = 0) in vec3 clip_position;

layout(location = 0) out vec2 clip_uv_coordinates;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    clip_uv_coordinates = clip_position.xy * 0.5 + 0.5;
    gl_Position         = vec4(clip_position, 1.0);
}
