// ///////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Logan Barnes - All Rights Reserved
// ///////////////////////////////////////////////////////////////////////////////////////
#pragma once

#include <filesystem>

namespace app::config
{

inline auto project_root_dir_path( ) -> std::filesystem::path
{
    return "@CMAKE_CURRENT_LIST_DIR@";
}

inline auto res_dir_path( ) -> std::filesystem::path
{
    return project_root_dir_path( ) / "res";
}

inline auto config_dir_path( ) -> std::filesystem::path
{
    return res_dir_path( ) / "config";
}

inline auto shader_dir_path( ) -> std::filesystem::path
{
    return res_dir_path( ) / "shaders";
}

} // namespace app::config
