// ///////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Logan Barnes - All Rights Reserved
// ///////////////////////////////////////////////////////////////////////////////////////
#pragma once

// external
#include <GL/gl3w.h>
#include <GLFW/glfw3.h>

// standard
#include <optional>

namespace app
{

class App
{
public:
    App( );
    ~App( );

    App( App const& )            = delete;
    App& operator=( App const& ) = delete;
    App( App&& )                 = delete;
    App& operator=( App&& )      = delete;

    auto run( ) -> int;

private:
    GLFWwindow* window_ = nullptr;

    GLuint program_ = 0;
    GLuint vbo_     = 0;
    GLuint vao_     = 0;

    float                  opacity_     = 0.5f;
    std::optional< float > old_opacity_ = std::nullopt;

    auto create_window( ) -> void;
    auto load_opengl( ) -> void;
    auto create_program( ) -> void;
    auto create_buffer( ) -> void;
    auto create_vertex_array( ) -> void;
};

} // namespace app
