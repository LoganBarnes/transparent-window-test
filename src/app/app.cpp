// ///////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Logan Barnes - All Rights Reserved
// ///////////////////////////////////////////////////////////////////////////////////////
#include "app/app.hpp"

// generated
#include "app/app_config.hpp"

// external
#include <GL/gl3w.h>
#include <GLFW/glfw3.h>
#include <Shadinclude/Shadinclude.hpp>
#include <spdlog/spdlog.h>

namespace app
{
namespace
{

auto create_shader( std::filesystem::path const& path, GLenum type ) -> GLuint
{
    if ( !std::filesystem::exists( path ) )
    {
        throw std::runtime_error( "File not found: " + path.string( ) );
    }

    // Load shader source from disk
    std::string shader_str    = Shadinclude::load( path.string( ) );
    char const* shader_source = shader_str.c_str( );

    auto id = glCreateShader( type );

    // Compile shader
    glShaderSource( id, 1, &shader_source, nullptr );
    glCompileShader( id );

    // Check shader
    auto result = GLint( GL_FALSE );
    glGetShaderiv( id, GL_COMPILE_STATUS, &result );

    if ( result == GL_FALSE )
    {
        auto log_length = GLint( 0 );
        glGetShaderiv( id, GL_INFO_LOG_LENGTH, &log_length );

        auto gl_error = std::vector< char >( static_cast< size_t >( log_length ) );
        glGetShaderInfoLog( id, log_length, nullptr, gl_error.data( ) );

        auto const err_msg
            = fmt::format( "Shader '{}' ({}): {}", path.filename( ).string( ), type, std::string( gl_error.data( ) ) );

        throw std::runtime_error( err_msg );
    }

    return id;
}

} // namespace

App::App( )
{
    create_window( );
    load_opengl( );
    create_program( );
    create_buffer( );
    create_vertex_array( );

    // Set the clear color alpha channel to zero.
    glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );

    // Enable blending so the alpha channel is used.
    glEnable( GL_BLEND );
    glBlendEquationSeparate( GL_FUNC_ADD, GL_FUNC_ADD );
    glBlendFuncSeparate( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO );

    // Change opacity with up and down arrows
    glfwSetWindowUserPointer( window_, this );
    glfwSetKeyCallback( window_, []( GLFWwindow* window, int key, int /*scancode*/, int action, int /*mods*/ ) {
        auto* self = static_cast< App* >( glfwGetWindowUserPointer( window ) );

        if ( key == GLFW_KEY_B && action == GLFW_RELEASE )
        {
            glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
            self->old_opacity_ = self->opacity_;
            self->opacity_     = 1.0;
        }

        if ( self->old_opacity_ )
        {
            if ( key == GLFW_KEY_T && action == GLFW_RELEASE )
            {
                glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
                self->opacity_     = self->old_opacity_.value( );
                self->old_opacity_ = std::nullopt;
            }
        }
        else
        {
            if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
            {
                glfwSetWindowShouldClose( window, GLFW_TRUE );
            }

            if ( key == GLFW_KEY_UP && action == GLFW_RELEASE )
            {
                self->opacity_ = std::min( 1.f, self->opacity_ + 0.1f );
            }

            if ( key == GLFW_KEY_DOWN && action == GLFW_RELEASE )
            {
                self->opacity_ = std::max( 0.f, self->opacity_ - 0.1f );
            }
        }
    } );
}

App::~App( )
{
    glDeleteVertexArrays( 1, &vao_ );
    glDeleteBuffers( 1, &vbo_ );
    glDeleteProgram( program_ );

    if ( window_ )
    {
        spdlog::debug( "Destroying GLFW window" );
        glfwDestroyWindow( window_ );
    }

    spdlog::debug( "Terminating GLFW" );
    glfwTerminate( );
}

auto App::run( ) -> int
{
    while ( !glfwWindowShouldClose( window_ ) )
    {
        glfwPollEvents( );

        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

        // Draw quad with the given opacity.
        glUseProgram( program_ );
        {
            auto const opacity_uniform_location = glGetUniformLocation( program_, "opacity" );
            glUniform1f( opacity_uniform_location, opacity_ );
        }
        glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );

        glfwSwapBuffers( window_ );
    }

    return EXIT_SUCCESS;
}

auto App::create_window( ) -> void
{
    // Set the error callback before any GLFW calls so we see when things go wrong.
    glfwSetErrorCallback( []( int /*error*/, char const* description ) {
        spdlog::error( "GLFW ERROR: {}", description );
    } );

    if ( glfwInit( ) == 0 )
    {
        throw std::runtime_error( "glfwInit() failed" );
    }
    spdlog::debug( "GLFW Initialized" );

    // Set window settings.
    glfwWindowHint( GLFW_RESIZABLE, GLFW_TRUE );
    glfwWindowHint( GLFW_VISIBLE, GLFW_TRUE );
    glfwWindowHint( GLFW_TRANSPARENT_FRAMEBUFFER, GLFW_TRUE );

    // Available in master (will be released in 3.4)
    // glfwWindowHint( GLFW_MOUSE_PASSTHROUGH, GLFW_TRUE );

    // Make sure we have a decent OpenGL Profile.
    glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );
    glfwWindowHint( GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 4 );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 0 ); // This is the highest MacOS will support.
#if defined( __APPLE__ ) || defined( WIN32 )
    glfwWindowHint( GLFW_CONTEXT_CREATION_API, GLFW_NATIVE_CONTEXT_API );
#else
    // Using EGL on linux allows us to display GUI apps from remote ssh machines.
    glfwWindowHint( GLFW_CONTEXT_CREATION_API, GLFW_EGL_CONTEXT_API );
#endif

    // Create fullscreen window.
    GLFWvidmode const* mode = glfwGetVideoMode( glfwGetPrimaryMonitor( ) );
    glfwWindowHint( GLFW_RED_BITS, mode->redBits );
    glfwWindowHint( GLFW_GREEN_BITS, mode->greenBits );
    glfwWindowHint( GLFW_BLUE_BITS, mode->blueBits );
    glfwWindowHint( GLFW_REFRESH_RATE, mode->refreshRate );

    window_ = glfwCreateWindow( mode->width, mode->height, "Transparency Test", nullptr, nullptr );
    if ( window_ == nullptr )
    {
        throw std::runtime_error( "GLFW window creation failed" );
    }
    spdlog::debug( "GLFW window created ({}x{})", mode->width, mode->height );

    glfwMakeContextCurrent( window_ );
    glfwSwapInterval( 1 );
}

auto App::load_opengl( ) -> void
{
    if ( gl3wInit( ) )
    {
        throw std::runtime_error( "Failed to load OpenGL via gl3wInit()" );
    }
    spdlog::debug( "OpenGL loaded" );

    auto const* vendor   = reinterpret_cast< char const* >( glGetString( GL_VENDOR ) );
    auto const* renderer = reinterpret_cast< char const* >( glGetString( GL_RENDERER ) );

    spdlog::info( "OpenGL Device: {} {}", vendor, renderer );
}

auto App::create_program( ) -> void
{
    auto const vert = create_shader( config::shader_dir_path( ) / "shader.vert", GL_VERTEX_SHADER );
    auto const frag = create_shader( config::shader_dir_path( ) / "shader.frag", GL_FRAGMENT_SHADER );

    program_ = glCreateProgram( );

    // Attach shaders and link program.
    glAttachShader( program_, vert );
    glAttachShader( program_, frag );

    glLinkProgram( program_ );

    // Detach shaders
    glDetachShader( program_, vert );
    glDetachShader( program_, frag );

    // Check program
    auto result = GLint( GL_FALSE );
    glGetProgramiv( program_, GL_LINK_STATUS, &result );

    if ( result == GL_FALSE )
    {
        auto log_length = GLint( 0 );
        glGetProgramiv( program_, GL_INFO_LOG_LENGTH, &log_length );

        auto gl_error = std::vector< char >( static_cast< size_t >( log_length ) );
        glGetProgramInfoLog( program_, log_length, nullptr, gl_error.data( ) );

        auto const err_msg = "Program: " + std::string( gl_error.data( ) );
        throw std::runtime_error( err_msg );
    }

    glDeleteShader( vert );
    glDeleteShader( frag );
}

auto App::create_buffer( ) -> void
{
    glGenBuffers( 1, &vbo_ );
    glBindBuffer( GL_ARRAY_BUFFER, vbo_ );

    // clang-format off
    auto const data = std::vector{
        -0.5f, -0.5f, 0.0f,
        +0.5f, -0.5f, 0.0f,
        -0.5f, +0.5f, 0.0f,
        +0.5f, +0.5f, 0.0f,
    };
    // clang-format on

    glBufferData(
        GL_ARRAY_BUFFER,
        static_cast< GLsizeiptr >( data.size( ) * sizeof( data.front( ) ) ),
        data.data( ),
        GL_STATIC_DRAW
    );
}

auto App::create_vertex_array( ) -> void
{
    glGenVertexArrays( 1, &vao_ );
    glBindVertexArray( vao_ );

    auto const maybe_clip_position_attribute_location = glGetAttribLocation( program_, "clip_position" );
    if ( maybe_clip_position_attribute_location < 0 )
    {
        throw std::runtime_error( "Cannot find attribute location for 'clip_position'" );
    }
    auto const clip_position_attribute_location = static_cast< GLuint >( maybe_clip_position_attribute_location );

    glEnableVertexAttribArray( clip_position_attribute_location );
    glVertexAttribPointer( clip_position_attribute_location, 3, GL_FLOAT, GL_FALSE, 0, 0 );
}

} // namespace app
