# Transparent Window Test

Testing GLFW/OpenGL transparent windows on MacOS, Windows, and Linux.

The program `TransparentWindow` creates a fullscreen transparent window with a semi-opaque quad at the center. Opacity
of the quad can be increased/decreased with the up/down arrow keys.

## Building

```bash
mkdir build
cd build
cmake ..
cmake --build .
```

## Running

### Single-config build systems:

```bash
./TransparentWindow
```

### Multi-config build systems:

```bash
./Debug/TransparentWindow
```

## Controls

|      Action      |    Key     |
|:----------------:|:----------:|
|       Quit       |   Escape   |
| Increase Opacity |  Up Arrow  |
| Decrease Opacity | Down Arrow |
